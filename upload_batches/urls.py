from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from upload_batches import views

urlpatterns = [
    path('my', views.MyUploadBatchesList.as_view(), name='upload_batches_my_all'),
    path('my/<int:id>', views.MyUploadBatch.as_view(), name='upload_batches_my_single'),
    path('my/<int:id>/recordings', views.MyUploadBatchRecordings.as_view(), name='upload_batches_my_single_recordings')
]

urlpatterns = format_suffix_patterns(urlpatterns)
