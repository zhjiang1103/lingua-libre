import { defineStore } from 'pinia'

export const useRecordWizardStore = defineStore('record-wizard', {
  state: () => ({
    locutors: [],
    selectedLocutorId: null,
    selectedLanguageId: null,
    words: [],
    recordings: {},
  }),
  getters: {
    locutor: (state) => state.locutors.find((locutor) => locutor.id === state.selectedLocutorId),
    hasRecordings: (state) => Object.values(state.recordings).length > 0,
    availableLanguages() {
      if (!this.locutor) return []
      return this.locutor['languages_used']
    },
  },
  actions: {
    removeSelectedLocutor() {
      if (!this.locutor) return

      let selectedLocutorIndex = this.locutors.findIndex((item) => item.id === this.locutor.id)
      this.locutors.splice(selectedLocutorIndex, 1)

      if (selectedLocutorIndex >= this.locutors.length) {
        selectedLocutorIndex = this.locutors.length - 1
      }

      this.selectedLocutorId = this.locutors[selectedLocutorIndex].id
    },
    addWords(input) {
      // Split by '#'
      const words = input.split('#')
      for (const word of words) {
        // Trim the word
        const trimmedWord = word.replace(/\t/g, '').trim()
        // ensure the word is valid
        if (trimmedWord === '') continue

        // ensure the word is not already in the list
        if (this.words.includes(trimmedWord)) continue

        // add to the list
        this.words.push(trimmedWord)
      }
    },
  },
})
