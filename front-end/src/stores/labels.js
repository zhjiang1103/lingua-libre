import { defineStore } from 'pinia'
import { useStorage } from '@vueuse/core'
import axios from 'axios'

const STORE_NAME = 'labels'

export const useLabelsStore = defineStore(STORE_NAME, {
  state: () => ({
    labels: useStorage(STORE_NAME, {}),
  }),
  getters: {},
  actions: {
    async fetchLabelForWikidataItem(qid, language = 'fr') {
      if (qid in this.labels) return // Do nothing if already here

      await axios
        .get('https://www.wikidata.org/wiki/Special:EntityData/' + qid + '.json')
        .then((response) => response.data)
        .then((response) => (this.labels[qid] = response.entities[qid].labels[language]?.value))
    },
  },
})
