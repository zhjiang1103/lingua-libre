from django.contrib import admin
from locutors.models import Locutor, UsesLanguage


class LocutorAdmin(admin.ModelAdmin):
    pass


class UsesLanguageAdmin(admin.ModelAdmin):
    pass


admin.site.register(Locutor, LocutorAdmin)
admin.site.register(UsesLanguage, UsesLanguageAdmin)
