from locutors.models import Locutor
from locutors.serializers import LocutorSerializer, UsesLanguageSerializer
from rest_framework import status
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.exceptions import NotFound, ValidationError
from django.core.exceptions import BadRequest


class LocutorList(APIView):
    """
    List all locutors
    """
    def get(self, request, format=None):
        locutors = Locutor.objects.all()
        serializer = LocutorSerializer(locutors, many=True)
        return Response(serializer.data)

class MyLocutorList(APIView):
    """
    List all locutors of the logged-in user
    """
    authentication_classes = [SessionAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        locutors = Locutor.objects.filter(linked_user=request.user.id)
        serializer = LocutorSerializer(locutors, many=True)
        return Response(serializer.data)
    
    def post(self, request, format=None):
        # TODO: Prevent settings is_main_locutor to true?
        # TODO: Return 409 Conflict if locutor with the same name exists?

        serializer = LocutorSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class LocutorDetail(APIView):
    """
    Retrieve a locutor instance
    """
    def get_locutor(self, id):
        try:
            return Locutor.objects.get(id=id)
        except Locutor.DoesNotExist:
            raise NotFound()
    
    def get(self, request, id, format=None):
        locutor = self.get_locutor(id)
        serializer = LocutorSerializer(locutor)
        return Response(serializer.data)

class MyLocutorDetail(APIView):
    """
    Retrieve a locutor instance of the logged-in user
    """
    authentication_classes = [SessionAuthentication]
    permission_classes = [IsAuthenticated]

    def get_locutor(self, request, id):
        try:
            return Locutor.objects.filter(linked_user=request.user.id).get(id=id)
        except Locutor.DoesNotExist:
            raise NotFound()
    
    def get(self, request, id, format=None):
        locutor = self.get_locutor(request, id)
        serializer = LocutorSerializer(locutor)
        return Response(serializer.data)

    def delete(self, request, id, format=None):
        locutor = self.get_locutor(request, id)
        try:
            locutor.delete()
        except BadRequest:
            raise ValidationError({"detail": "Cannot delete this Locutor."})
        return Response({"detail": "Locutor deleted successfully."}, status=status.HTTP_204_NO_CONTENT)

class MyLocutorLanguages(APIView):
    """
    Languages of a logged-in user's locutor.
    """
    authentication_classes = [SessionAuthentication]
    permission_classes = [IsAuthenticated]

    def get_locutor(self, request, id):
        try:
            return Locutor.objects.filter(linked_user=request.user.id).get(id=id)
        except Locutor.DoesNotExist:
            raise NotFound()
    
    def post(self, request, id, format=None):
        locutor = self.get_locutor(request, id)
        context = { 'locutor': locutor }

        serializer = UsesLanguageSerializer(data=request.data, context=context)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
